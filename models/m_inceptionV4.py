
import os
import numpy as np
import tensorflow as tf

from models.tfslim.research.slim.nets import inception
from models.model import BaseModel
from tensorflow.contrib import slim

class ModelInceptionV4(BaseModel):
    def __init__(self, options):
        self.options = options


    def build(self, imInput):
        # Create the model, use the default arg scope to configure the batch norm parameters.
        with slim.arg_scope(inception.inception_v4_arg_scope()):
            logits, _ = inception.inception_v4(imInput, num_classes=101, is_training=True, create_aux_logits=False)
            return logits, _

    def load_weights(self, pathToCheckpoint):
        """Returns a function run by the chief worker to warm-start the training."""
        checkpoint_exclude_scopes=["InceptionV4/Logits", "InceptionV4/AuxLogits"]

        exclusions = [scope.strip() for scope in checkpoint_exclude_scopes]

        variables_to_restore = []
        for var in slim.get_model_variables():
            excluded = False
            for exclusion in exclusions:
                if var.op.name.startswith(exclusion):
                    excluded = True
                    break
            if not excluded:
                variables_to_restore.append(var)

        return slim.assign_from_checkpoint_fn(pathToCheckpoint, variables_to_restore)
