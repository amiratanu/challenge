#!/usr/bin/python3
import sys
sys.path.append('../../activity-detection/src/')

import numpy as np
import csv

from datasets.dataset import BaseDB
from datasets.video import Video

class CHARADES(BaseDB):
    def __init__(self, options):
        self.options = options

        self.classes = []
        self.videos  = {'train':[], 'test':[]}

        self.readDirectories()
        #self.loadingBatch = 0
    def readDirectories(self):
        typeData  = options.get('experiment','typeData')
        nVidTrAux = 0
        nVidTeAux = 0
        self.trainIndex = []
        self.testIndex  = []
        print("Read training directories to memory 0%",end='\r',flush=True)
        with open(self.options.get('charades','train_csv')) as f:
            reader = csv.DictReaderf
            for row in reader:
                if row['actions'] is not '':
                    actions = row['actions'].split(';')
                    for action in actions:
                        aux = action.split(' ')
                        c_ = aux[0]
                        self.classes.append(c_)
                        init = float(aux[1])
                        end  = float(aux[2])
                        length = float(row['length'])
                        if end > length:
                            end = length
                        if typeData == 'RGB':
                            imageAux = Video(options.get('charades','rgb') + row['id'], c_, id=row['id'], init=init, end=end)

                        self.videos['train'].append(imageAux)
                        for it in range(imageAux.nImages):
                            self.trainIndex.append(nVidTrAux)
                nVidTrAux += 1
                print("Read training directories to memory {:.2f}%".format(((nVidTrAux)/7985)*100),flush=True,end='\r')

        print("\nRead testing directories to memory 0%",end='\r',flush=True)

        with open(self.options.get('charades','test_csv')) as f:
            reader = csv.DictReader(f)
            for row in reader:
                if row['actions'] is not '':
                    actions = row['actions'].split(';')
                    for action in actions:
                        aux = action.split(' ')
                        c_ = aux[0]
                        init = float(aux[1])
                        end  = float(aux[2])
                        if typeData == 'RGB':
                            imageAux = Video(options.get('charades','rgb') + row['id'], c_, id=row['id'], init=init, end=end)
                        self.videos['test'].append(imageAux)
                        for it in range(imageAux.nImages):
                            self.testIndex.append(nVidTeAux)
                nVidTeAux += 1
                print("Read testing directories to memory {:.2f}%".format(((nVidTeAux)/1863)*100),flush=True,end='\r')

        self.nVidTr   = len(self.videos['train'])
        self.nVidTe   = len(self.videos['test'])
        self.nVideos  = self.nVidTr + self.nVidTe
        self.classes  = np.unique(self.classes)
        self.nClasses = len(self.classes)

        self.nFrameTr = 0
        self.nFrameTe = 0

        for video in self.videos['train']:
            self.nFrameTr += video.nImages

        for video in self.videos['test']:
            self.nFrameTe += video.nImages

        self.nFrames  = self.nFrameTr + self.nFrameTe
        print("\n# videos: {}\n# videos train: {}\n# videos test: {}\n".format(self.nVideos, self.nVidTr, self.nVidTe))
        print("# frames: {}\n# frames train: {}\n# frames test: {}\n".format(self.nFrames, self.nFrameTr, self.nFrameTe))
        print("# classes: {}".format(self.nClasses))

        self.trainIndex = np.array(self.trainIndex)
        self.testIndex  = np.array(self.testIndex)
        np.random.seed(4)
        np.random.shuffle(self.trainIndex)
        np.random.shuffle(self.testIndex)

if __name__ == '__main__':
    import time
    import params
    (options, args) = params.set()
    data = CHARADES(options)
